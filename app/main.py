import requests
from fastapi import FastAPI, Request
from requests.api import patch

from asf_policies import doi_policy, member_policy

HEADERS = {"User-Agent": "asf-demo; mailto:labs@crossref.org"}

app = FastAPI()


def patch_asf(record):
    policy = doi_policy(record["DOI"]) or member_policy(record["member"])
    if policy:
        licenses = record.get("license", [])
        licenses.append({"content-version": "asf", "URL": policy})
        record["license"] = licenses
    return record


def patch_single_work(response):
    response["message"] = patch_asf(response["message"])
    return response


def patch_work_list(response):
    new_items = [patch_asf(item) for item in response["message"]["items"]]
    response["message"]["items"] = new_items
    return response


def patch_works(response):
    if response["message-type"] == "work":
        return patch_single_work(response)
    if response["message-type"] == "work-list":
        return patch_work_list(response)

    return response


def echo_url(r):
    try:
        path = r.url.path
        params = str(r.query_params)
        return requests.get(
            f"https://api.crossref.org{path}", params=params, headers=HEADERS
        ).json()

    except Exception as e:
        raise e


@app.get("/works/{doi:path}")
async def works(request: Request):
    return patch_works(echo_url(request))


@app.get("/members{member_id:path}")
async def members(request: Request):
    return patch_works(echo_url(request))


@app.get("/types{member_id:path}")
async def members(request: Request):
    return patch_works(echo_url(request))


@app.get("/journals{member_id:path}")
async def members(request: Request):
    return patch_works(echo_url(request))


@app.get("/")
async def works():
    return "asf-demo"
