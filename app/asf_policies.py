
member_policies = {

    '78':'https://elsevier-default-asf-policy',
    '316':'https://acs-default-asf-policy'
}

doi_policies = {
    '10.1016/j.ijrobp.2020.07.298':'https://elsevier-doi-specific-asf-policy',
    '10.1021/acsmaterialslett.9b00333':'https://acs-doi-specific-asf-policy',
    '10.5555/12345678':'https://psychoceramics-doi-specific-asf-policy'
}

def member_policy(member_id):
    """
    obviously, this can lookup from database if needed for demo
    """
    return member_policies.get(member_id,None)

def doi_policy(doi):
    """
    obviously, this can lookup from database if needed for demo
    """
    return doi_policies.get(doi,None)



if __name__ == "__main__":
    policy = doi_policy('10.5555/1234567x') or member_policy('788')
    print(policy)


