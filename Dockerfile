# Use an official Python runtime as a parent image
#FROM python:3.9-alpine
FROM python:3.8.5-slim-buster
#FROM tiangolo/uvicorn-gunicorn-starlette:python3.7
#FROM tiangolo/uvicorn-gunicorn:python3.8-alpine3.10

# Set environment varibles
#ENV FLASK_APP=app.py
#ENV FLASK_RUN_HOST=0.0.0.0
#ENV PYTHONDONTWRITEBYTECODE 1
#ENV PYTHONUNBUFFERED 1
#RUN apk add --no-cache gcc musl-dev linux-headers

COPY requirements.txt requirements.txt
RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

#COPY . .
COPY ./app /app
WORKDIR /app

# Setup directory for prometheus metrics
#RUN rm -rf /tmp/multiproc-tmp && mkdir /tmp/multiproc-tmp
#ENV prometheus_multiproc_dir=/tmp/multiproc-tmp
# ENV WEB_CONCURRENCY="1"
# ENV BIND="0.0.0.0:5000"

EXPOSE 5000

# default run, can be overridden in docker compose.
#CMD gunicorn demoproject.wsgi:application --bind 0.0.0.0:8000 --workers 3
#CMD ["flask", "run"]
#CMD gunicorn  app:app -w 1 --threads 1 -b 0.0.0.0:5000
#CMD gunicorn -w 1 --threads 1 -b 0.0.0.0:5000 wsgi:app
#CMD ["python","app/main.py"]
#CMD gunicorn -w 4 -k uvicorn.workers.UvicornWorker --log-level warning wsgi:app
CMD ["uvicorn","--log-level", "info","--workers","1","--host", "0.0.0.0","--port", "5000","--workers","1", "main:app"]

