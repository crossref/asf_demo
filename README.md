# asf_demo

This proxy api server demonstrates asf metdata patched into real Crossref API results.

It is hosted on labs.crossref.org.


Currently the proxy patches based on two static lists. these could easily be made into a sqlite db instead.

There are two ways to add a policy to a DOI

- per DOI
- per publisher

per-DOI always supercedes per-publisher. this means a publisher could assign a default policy and override it on a per-DOI basis.

The demonstrator contains default mappings for Elseveier (`member_id=78`) and ACS (`member_id=316`.)

It has DOI-specific overrides for Elsevier DOI "10.1016/j.ijrobp.2020.07.298" and ACS DOI "10.1021/acsmaterialslett.9b00333". It also has a DOI-specific policy for a document in our sample "Journal of Psychoceramics" 

The two lists are coded below. Obviously the sample policy URIs would be replaced by the apporpriate STM policy URIs:

```
member_policies = {

    '78':'https://acs-default-asf-policy',
    '316':'https://acs-default-asf-policy'
}

doi_policies = {
    '10.1016/j.ijrobp.2020.07.298':'https://elsevier-doi-specific-asf-policy',
    '10.1021/acsmaterialslett.9b00333':'https://acs-doi-specific-asf-policy',
    '10.5555/12345678':'https://psychoceramics-doi-specific-asf-policy'
}

```
## Examples:

NB: These results do not represent the final json schema, they are simply illustrative of the proxy technique.

The following should list sample Elsevier DOI with a DOI-specific policy.

```
http://api.labs.crossref.org/works/10.1016/j.ijrobp.2020.07.298
```

The following should list a random sample of Elsevier DOIs with a default policy.

```
http://api.labs.crossref.org/members/78/works?sample=10
```

The following should list sample ACS DOI with a DOI-specific policy.

```
http://api.labs.crossref.org/works/10.1021/acsmaterialslett.9b00333
```

The following should list a random sample of ACS DOIs with a default policy.

```
http://api.labs.crossref.org/members/316/works?sample=10
```



